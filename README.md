# etcd-rpm-el6

Rebuild of the Fedora Core 22 etcd RPM available from the Fedora build servers for use with el6 distributions (RHEL 6, CentOS 6 etc.).


Changes include:
* Removal of systemd references and unit files
* Removal of /etc/etcd.conf configuration layout
* Removal of references/cases for Fedora specific configuration
* Removal of Fedora specific patch file
* Creation of el6 init file
* Creation of logrotate configuration file
* Creation of /etc/sysconfig/etcd configuration file


Note:
* Changes to the spec file have been made to build el6 rpms and this has more than likely broken other platform builds completely for this spec file
* Original tarball build script (<2.0.12) patched to remove git requirement (git SHA will no longer be included in etcd --version output)
* Binary packages (<2.2.0-2) were built (static) against golang 1.4.2 RPM built from Fedora Core 21 SRPM
* Binary packages (>=2.2.0-2) are built (static) against golang 1.5.1 el6 RPM from Fedora koji build servers
* Build was on CentOS 6.6
* Configuration is default out of the box and was built for PoC purposes, it may not have sane configuration parameters for production

## Source and compiled RPMs
Source and compiled RPMs (x86_64) built from this repo can be found at the following location **USE AT YOUR OWN RISK**

**x86_64 RPMs**
* [etcd-2.2.0-2.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/etcd-2.2.0-2.el6.x86_64.rpm)
* [etcd-2.2.0-1.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/etcd-2.2.0-1.el6.x86_64.rpm)
* [etcd-2.1.3-1.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/etcd-2.1.3-1.el6.x86_64.rpm)
* [etcd-2.1.2-1.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/etcd-2.1.2-1.el6.x86_64.rpm)
* [etcd-2.1.1-1.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/etcd-2.1.1-1.el6.x86_64.rpm)
* [etcd-2.1.0rc.0-1.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/etcd-2.1.0rc.0-1.el6.x86_64.rpm)
* [etcd-2.0.13-1.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/etcd-2.0.13-1.el6.x86_64.rpm)
* [etcd-2.0.12-1.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/etcd-2.0.12-1.el6.x86_64.rpm)
* [etcd-2.0.11-5.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/etcd-2.0.11-5.el6.x86_64.rpm)

**SRPMs**
* [etcd-2.2.0-2.el6.src.rpm](http://www.pixeldrift.net/rpm/SRPMS/etcd-2.2.0-2.el6.src.rpm)
* [etcd-2.2.0-1.el6.src.rpm](http://www.pixeldrift.net/rpm/SRPMS/etcd-2.2.0-1.el6.src.rpm)
* [etcd-2.1.3-1.el6.src.rpm](http://www.pixeldrift.net/rpm/SRPMS/etcd-2.1.3-1.el6.src.rpm)
* [etcd-2.1.2-1.el6.src.rpm](http://www.pixeldrift.net/rpm/SRPMS/etcd-2.1.2-1.el6.src.rpm)
* [etcd-2.1.1-1.el6.src.rpm](http://www.pixeldrift.net/rpm/SRPMS/etcd-2.1.1-1.el6.src.rpm)
* [etcd-2.0.13-1.el6.src.rpm](http://www.pixeldrift.net/rpm/SRPMS/etcd-2.0.13-1.el6.src.rpm)
* [etcd-2.0.12-1.el6.src.rpm](http://www.pixeldrift.net/rpm/SRPMS/etcd-2.0.12-1.el6.src.rpm)
* [etcd-2.0.11-5.el6.src.rpm](http://www.pixeldrift.net/rpm/SRPMS/etcd-2.0.11-5.el6.src.rpm)